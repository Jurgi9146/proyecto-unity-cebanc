﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutomaticDoorOpen : MonoBehaviour
{
    public Transform _FPCTransform;
    private Transform _DoorTransform;
    private Vector3 _DoorOpen;
    private Quaternion _DoorOpenQuaternion;
    public int _openDegree;
    private AudioSource _doorAudioSource;
    private bool _fistRoutine = true;
    // Use this for initialization
    void Start()
    {
        _doorAudioSource = GetComponent<AudioSource>();
        _DoorTransform = GetComponent<Transform>();
        //Door final position
        _DoorOpen = new Vector3(_DoorTransform.rotation.eulerAngles.x, _DoorTransform.rotation.eulerAngles.y - _openDegree, _DoorTransform.rotation.eulerAngles.z);
        _DoorOpenQuaternion = Quaternion.Euler(_DoorOpen);
    }

    // Update is called once per frame
    void Update()
    {
        OpenDoor();
    }

    void OpenDoor()
    {
        if (Vector3.Distance(_FPCTransform.position, _DoorTransform.position) <= 3f) //If FPC is closer than 3 from door
        {
            //make door open
            _DoorTransform.rotation = Quaternion.RotateTowards(_DoorTransform.rotation, _DoorOpenQuaternion, 50 * Time.deltaTime);
            //start door opening audio corroutine
            if (_fistRoutine == true)
            {
                StartCoroutine(AudioPlay());
                _fistRoutine = false;
            }
        }
    }

    //Audio corroutine. Play audio once
    IEnumerator AudioPlay()
    {
        _doorAudioSource.Play();
        yield return null;
    }

    
}
