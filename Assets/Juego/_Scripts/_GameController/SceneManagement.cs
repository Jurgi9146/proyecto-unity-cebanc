﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagement : MonoBehaviour {

    public int thisLevel;
    private int nextLevel;

    public void ReloadLevel()
    {
        SceneManager.LoadScene(thisLevel);
    }

    public void NextLevel()
    {
        nextLevel = thisLevel + 1;

        if (nextLevel == 5)
        {
            FindObjectOfType<GameController>().UnableFpsController();
        }      
            SceneManager.LoadScene(nextLevel);      
    }

    public void BackToMenu()
    {

        SceneManager.LoadScene(0);
    }

    public void SalirJuego()
    {
        Application.Quit();
    }

    
}
