﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralInformationEnumeration : MonoBehaviour {

    public enum Information
    {
        PilarPuzzle,
        BrazierPuzzle,
        EarthCreationPuzzle,
        DestroyNailsPuzzle,
        VaseInfo,
        BrazierRoomPuzze,
    }
}
