﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralInformationTrigger : MonoBehaviour {

    public delegate void GeneralTrigger(bool onTrigger, int action);
    public static event GeneralTrigger OnGeneralTrigger;
    private bool _onTrigger;
    public int _action;

    private void Awake()
    {
        OnGeneralTrigger = null;
    }

    private void OnTriggerEnter(Collider other)
    {
        _onTrigger = true;

        if (OnGeneralTrigger != null)
        {
            OnGeneralTrigger(_onTrigger, _action);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        _onTrigger = false;
        if (OnGeneralTrigger != null)
        {
            OnGeneralTrigger(_onTrigger, _action);
        }
    }
}
