﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class GameController : MonoBehaviour
{
    [Header ("Amount of elements active")]
    public int _allowedElements;
    private bool _ElementObtained;
    private bool _allowPanel;
    //Enumerations
    [HideInInspector]
    public int UsingElement = 0;
    //Canvas configuration
    [Header("Power image configuration")]
    public  Image _EarthImage;
    public Image _FireImage;
    public Image _AirImage;
    public Image _WaterImage;
    [Header("Keyboard Configuration")]
    public GameObject _KeyboardInfo;
    [Header("Element Power Using Info Panel Setting")]
    public GameObject _InfoPanel;
    [Header("Death Panel")]
    public GameObject _DeathPanel;
    [Header("Final Door Panel")]
    public GameObject _FinalDoorPanel;

    [Header ("Level 1  *******")]
    public GameObject _pilarTriggerInfo;
    [Header("Level 2  *******")]
    public GameObject _BrazzierPuzzleInfo;
    [Header("Level 3  *******")]
    public GameObject _MouseInformation;
    public GameObject _EarthCreationPuzzle;
    public GameObject _DestroyNailsPuzzle;
    public GameObject _brazierRoomPuzzle;
    [Header("Level 4  *******")]
    public GameObject _VaseInfo;

    [Header("Colour Canvas Configuration")]
    public Color nonGetColor;
    public Color selectedColor;
    public Color nonSelectedColor;

    private AudioSource changeElement;


    private void Start()
    {
        //Add-on the events
        ElementPowerScript EPS = FindObjectOfType<ElementPowerScript>();
        EPS.OnObtainedElement += EPS_OnObtainedElement;
        DeathFPC DFPC = FindObjectOfType<DeathFPC>();
        DFPC.OnFPCDead += DFPC_OnFPCDead;
        FinalDoorTrigger FDT = FindObjectOfType<FinalDoorTrigger>();
        FDT.OnFinalPanel += FDT_OnFinalPanel;
        GeneralInformationTrigger.OnGeneralTrigger += GIT_OnGeneralTrigger;
        AirMechanismGrab.OnFirstInfoEvent += AirMechanismGrab_OnFirstInfoEvent;
        changeElement = GetComponent<AudioSource>();
    }

    //Event after getting the element: canvas configuration & allow to use the different powers
    private void EPS_OnObtainedElement(bool elementObtained)
    {       
        ElementImageCanvas(); //Canvas actualization
        StartCoroutine(InfoPanel());
        _ElementObtained = elementObtained;
    }

    //Mouse information - for air mechanism
    private void AirMechanismGrab_OnFirstInfoEvent()
    {
        _MouseInformation.SetActive(true);
        StartCoroutine(WaitForInfo());
    }

    //General canvas information (from differente triggers)
    private void GIT_OnGeneralTrigger(bool onTrigger, int action)
    {
        if (onTrigger == true)
        {
            if (action == (int)GeneralInformationEnumeration.Information.PilarPuzzle) //Activate final door panel. Earth Level
            {
                _pilarTriggerInfo.SetActive(true);
            }
            if (action == (int)GeneralInformationEnumeration.Information.BrazierPuzzle) //Activate brazzier puzzle panel. Fire Level
            {
                _BrazzierPuzzleInfo.SetActive(true);
            }
            if (action == (int)GeneralInformationEnumeration.Information.EarthCreationPuzzle) //Activate Earth creation puzzle panel. Air Level
            {
                _EarthCreationPuzzle.SetActive(true);
            }
            if (action == (int)GeneralInformationEnumeration.Information.DestroyNailsPuzzle) //Activate Destroy nails puzzle panel. Air Level
            {
                _DestroyNailsPuzzle.SetActive(true);
            }
            if (action == (int)GeneralInformationEnumeration.Information.VaseInfo) //Activate vase information. Water Level
            {
                _VaseInfo.SetActive(true);
            }
            if (action == (int)GeneralInformationEnumeration.Information.BrazierRoomPuzze) //Activate brazier room puzzle. Air Level
            {
                _brazierRoomPuzzle.SetActive(true);
            }
        }
        if (onTrigger == false) //set inactive all the panels
        {
            _pilarTriggerInfo.SetActive(false);
            _BrazzierPuzzleInfo.SetActive(false);
            _EarthCreationPuzzle.SetActive(false);
            _DestroyNailsPuzzle.SetActive(false);
            _VaseInfo.SetActive(false);
            _brazierRoomPuzzle.SetActive(false);
        }
    }

    //Event final panel: "Press E to open door: (load next level)"
    private void FDT_OnFinalPanel(bool panel)   
    {       
        _allowPanel = panel;
        if (panel == true)
        {
            _FinalDoorPanel.SetActive(true);       
        }
        else
        {
            _FinalDoorPanel.SetActive(false);
        }
    }

    //Event FPC is dead: Either bone shoot or fall to infinty
    private void DFPC_OnFPCDead()
    {
        UnableFpsController(); //Make FPSController Unable
        _DeathPanel.SetActive(true); //Activate death panel
        
    }

    //Make FPC unable after death
    public void UnableFpsController()
    {
        StartCoroutine(DisableFPS());
    }

    private IEnumerator DisableFPS()
    {
        yield return new WaitForSeconds(0.1f);
        CharacterController characterController = FindObjectOfType<CharacterController>().GetComponent<CharacterController>();
        FirstPersonController fpsController = FindObjectOfType<FirstPersonController>().GetComponent<FirstPersonController>();

        characterController.enabled = false;
        fpsController.enabled = false;


        // Allow it to move on the screen
        Cursor.lockState = CursorLockMode.None;
        // Show the cursor
        Cursor.visible = true;
    }



    private void Update()
    {
        MechanismEnumeration(); //mechanism enumeration update
        CanvasConfiguration(); //Canvas update
        if (_ElementObtained ==false)
        {
            DefaultElementCanvasConfiguration(); //Show element canvas default configuration, when the level start
        }
        if (_allowPanel == true)
        {
            LoadNextScene();
        }
    }

    //Select which power is currently the player using: Using Element
    void MechanismEnumeration()
    {
        if (_ElementObtained ==true) //after getting the power
        {
            if (Input.GetKeyDown(KeyCode.Alpha1) && _allowedElements >= 1) //allowed elements = 1; just Earth. Level 1
            {
                changeElement.Play();
                UsingElement = (int)ElementsEnumerationScript.Elements.Earth; //Press 1 -> variable actualization Earth
                //Debug.Log("using Earth");
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2) && _allowedElements >= 2) //Allowed elements = 2. Earth, fire. Level 2
            {
                changeElement.Play();
                UsingElement = (int)ElementsEnumerationScript.Elements.Fire; //press 2 -> variable actualization Fire
                //Debug.Log("Using fire");
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3) && _allowedElements >= 3) //Allowed elements = 3. Earth, fire, air. level 3
            {
                changeElement.Play();
                UsingElement = (int)ElementsEnumerationScript.Elements.Air; //Press 3 -> variable actualization to Air
                //Debug.Log("using air");
            }
            else if (Input.GetKeyDown(KeyCode.Alpha4) && _allowedElements >= 4) //allowe elemntes = 4. Earth, fire, air, water. Level 4
            {
                changeElement.Play();
                UsingElement = (int)ElementsEnumerationScript.Elements.Water; //Press 4-> variable actualiation to Water
            }
        }
        
    }

    //Canvas configuration when switching between element powers
    void CanvasConfiguration()
    {
        if (_ElementObtained == true) //After getting the element
        {
            if (_allowedElements ==1) //If only one element is allowed. Level 1. 
            {
                if (UsingElement == (int)ElementsEnumerationScript.Elements.Earth) //When Earth element is selected
                {
                    _EarthImage.color = Color.Lerp(nonSelectedColor, selectedColor, 5); //make canvas image colored
                }
            }
            if (_allowedElements == 2) //if two elements are allowed. Level 2
            {
                if (UsingElement == (int)ElementsEnumerationScript.Elements.Earth) //when Earth element is selected
                {
                    _EarthImage.color = Color.Lerp(nonSelectedColor, selectedColor, 5); //make earth canvas image colored
                    _FireImage.color = Color.Lerp(selectedColor, nonSelectedColor, 5); //make fire canvas image transparent
                }
                if (UsingElement == (int)ElementsEnumerationScript.Elements.Fire) //when Fire is selected
                {
                    _EarthImage.color = Color.Lerp(selectedColor, nonSelectedColor, 5); //make earth canvas image colored
                    _FireImage.color = Color.Lerp(nonSelectedColor, selectedColor, 5);//make fire canvas image transparent
                }
            }

            if (_allowedElements == 3) //if three elements are allowed. Level3
            {
                if (UsingElement == (int)ElementsEnumerationScript.Elements.Earth) //when Earth element is selected
                {
                    _EarthImage.color = Color.Lerp(nonSelectedColor, selectedColor, 5);//make earth canvas image colored
                    _FireImage.color = Color.Lerp(selectedColor, nonSelectedColor, 5);//make fire canvas image transparent
                    _AirImage.color = Color.Lerp(selectedColor, nonSelectedColor, 5);//make air canvas image transparent
                }
                if (UsingElement == (int)ElementsEnumerationScript.Elements.Fire)//when Fire element is selected
                {
                    _EarthImage.color = Color.Lerp(selectedColor, nonSelectedColor, 5);// make earth canvas image transparent
                    _FireImage.color = Color.Lerp(nonSelectedColor, selectedColor, 5);//make fire canvas image colored
                    _AirImage.color = Color.Lerp(selectedColor, nonSelectedColor, 5);// make air canvas image transparent
                }
                if (UsingElement == (int)ElementsEnumerationScript.Elements.Air) //when Air element is selected
                {
                    _EarthImage.color = Color.Lerp(selectedColor, nonSelectedColor, 5); //// make earth canvas image transparent
                    _FireImage.color = Color.Lerp(selectedColor, nonSelectedColor, 5);//make fire canvas image transparent
                    _AirImage.color = Color.Lerp(nonSelectedColor, selectedColor, 5);// make air canvas image  colored
                }
            }
            if (_allowedElements == 4) //if four elemntes are allowed. Level 4
            {
                if (UsingElement == (int)ElementsEnumerationScript.Elements.Earth) //when eart element is selected
                {
                    _EarthImage.color = Color.Lerp(nonSelectedColor, selectedColor, 5);//make earth canvas image colored
                    _FireImage.color = Color.Lerp(selectedColor, nonSelectedColor, 5);//make fire canvas image transparent
                    _AirImage.color = Color.Lerp(selectedColor, nonSelectedColor, 5);// make air canvas image transparent
                    _WaterImage.color = Color.Lerp(selectedColor, nonSelectedColor, 5);// make water canvas image transparent
                }
                if (UsingElement == (int)ElementsEnumerationScript.Elements.Fire)//when Fire element is selected
                {
                    _EarthImage.color = Color.Lerp(selectedColor, nonSelectedColor, 5);//make earth canvas image transparent
                    _FireImage.color = Color.Lerp(nonSelectedColor, selectedColor, 5);//make fire canvas image colored
                    _AirImage.color = Color.Lerp(selectedColor, nonSelectedColor, 5);// make air canvas image transparent
                    _WaterImage.color = Color.Lerp(selectedColor, nonSelectedColor, 5);// make water canvas image transparent
                }
                if (UsingElement == (int)ElementsEnumerationScript.Elements.Air)//when Air element is selected
                {
                    _EarthImage.color = Color.Lerp(selectedColor, nonSelectedColor, 5);//make earth canvas image transparent
                    _FireImage.color = Color.Lerp(selectedColor, nonSelectedColor, 5);//make fire canvas image transparent
                    _AirImage.color = Color.Lerp(nonSelectedColor, selectedColor, 5);// make air canvas image colored
                    _WaterImage.color = Color.Lerp(selectedColor, nonSelectedColor, 5);// make water canvas image transparent
                }
                if (UsingElement == (int)ElementsEnumerationScript.Elements.Water)//when water element is selected
                {
                    _EarthImage.color = Color.Lerp(selectedColor, nonSelectedColor, 5);//make earth canvas image transparent
                    _FireImage.color = Color.Lerp(selectedColor, nonSelectedColor, 5);//make fire canvas image transparent
                    _AirImage.color = Color.Lerp(selectedColor, nonSelectedColor, 5);// make air canvas image transparent
                    _WaterImage.color = Color.Lerp(nonSelectedColor, selectedColor, 5);// make water canvas image colored
                }
            }

        }
    }

    //Canvas configuration when getting the power (just called once per element). Make image appear in canvas
    void ElementImageCanvas()
    {
        if (_allowedElements == 1) //Level 1
        {
            _EarthImage.color = Color.Lerp(nonGetColor, nonSelectedColor, 5f); //make Earth image appear in canvas
        }
        if (_allowedElements == 2) //Level 2
        {
            _FireImage.color = Color.Lerp(nonGetColor, nonSelectedColor, 5f); //make Fire image appear in canvas
        }
        if (_allowedElements == 3) //level 3
        {
            _AirImage.color = Color.Lerp(nonGetColor, nonSelectedColor, 5f); //make air image appear in canvas
        }
        if (_allowedElements == 4) //level 4
        {
            _WaterImage.color = Color.Lerp(nonGetColor, nonSelectedColor, 5f); //make water image appear in canvas
        }
    }

    //Canvas default(previous information) configuration when going to next level
    void DefaultElementCanvasConfiguration()
    {
        {
            if (_allowedElements == 2) //Level 2
            {
                _EarthImage.color = Color.Lerp(nonSelectedColor, selectedColor, 5); //Earth power image showed in canvas by default. Transparent
            }
            if (_allowedElements == 3) //Level 3
            {
                _EarthImage.color = Color.Lerp(nonSelectedColor, selectedColor, 5);//Earth power image showed in canvas by default. Transparent
                _FireImage.color = Color.Lerp(nonGetColor, nonSelectedColor, 5f);//Fire power image showed in canvas by default. Transparent
            }
            if (_allowedElements == 4) //Level 4
            {
                _EarthImage.color = Color.Lerp(nonSelectedColor, selectedColor, 5);//Earth power image showed in canvas by default. Transparent
                _FireImage.color = Color.Lerp(nonGetColor, nonSelectedColor, 5f);//Fire power image showed in canvas by default. Transparent
                _AirImage.color = Color.Lerp(nonGetColor, nonSelectedColor, 5f);//Air power image showed in canvas by default. Transparent
            }
        }
    }

    //Load next scene
    void LoadNextScene()
    {
        if (Input.GetKey(KeyCode.E))
        {
            SceneManagement SM = FindObjectOfType<SceneManagement>().GetComponent<SceneManagement>(); //Call scene management script
            SM.NextLevel();
            _allowPanel = false;
        }
    }

    //Delays for information panels
    IEnumerator InfoPanel()
    {
        _KeyboardInfo.SetActive(true);
        yield return new WaitForSeconds(2f);
        _KeyboardInfo.SetActive(false);
        _InfoPanel.SetActive(true);
        yield return new WaitForSeconds(2f);
        _InfoPanel.SetActive(false);
    }
    IEnumerator WaitForInfo()
    {
        yield return new WaitForSeconds(3f);
        _MouseInformation.SetActive(false);
    }

}

