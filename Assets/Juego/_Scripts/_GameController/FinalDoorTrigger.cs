﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinalDoorTrigger : MonoBehaviour {

    public delegate void FinalPanel(bool panel);
    public event FinalPanel OnFinalPanel;
    public bool _panelOn;

    private void Awake()
    {
        OnFinalPanel = null;
    }
    private void OnTriggerEnter (Collider other)
    {
        _panelOn = true;

        if (OnFinalPanel!= null)
        {
            OnFinalPanel (_panelOn);
        }

    }

    private void OnTriggerExit(Collider other)
    {
        _panelOn = false;
        if (OnFinalPanel != null)
        {
            OnFinalPanel (_panelOn);
        }
    }
}
