﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathFPC : MonoBehaviour {
    public delegate void FPCDead();
    public event FPCDead OnFPCDead;

    private void Awake()
    {
        OnFPCDead = null;
    }

    //Call the event when falling from the earth floor puzzle
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "DeathTrigger")
        {
            if (OnFPCDead != null)
            {
                OnFPCDead();
            }
        }
    }
    //Call the event when colliding with a bone
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bone")
        {
            if (OnFPCDead != null)
            {
                OnFPCDead();
            }
        }
    }
}
