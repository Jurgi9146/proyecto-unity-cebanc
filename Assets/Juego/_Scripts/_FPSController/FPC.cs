﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class FPC : MonoBehaviour
{

    //Raycast Information
    [Header("Raycast Setting")]
    public Transform RaycastOrigin;
    public Transform _grabPosition;
    private float _distanceWithObject;

    private CharacterController _miCharacterController;
    private FirstPersonController _miFirstPersonController;
    private CapsuleCollider _miCapsuleCollider;

    private Rigidbody miRigidBody;

    private GameController _usingElement;
    

    private void Start()
    {
        miRigidBody = GetComponent<Rigidbody>();

        _miFirstPersonController = GetComponent<FirstPersonController>();
        _miCharacterController = GetComponent<CharacterController>();
        _miCapsuleCollider = GetComponent<CapsuleCollider>();

        _usingElement = FindObjectOfType<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        //Call raycast
        Raycast();
    }

    void Raycast()
    {
        if (Input.GetKey(KeyCode.E))
        {
            RaycastHit hitInfo;

            if (_usingElement.UsingElement > 0)
            {
                
                if (Physics.SphereCast(RaycastOrigin.position, 0.25f, RaycastOrigin.forward, out hitInfo, Mathf.Infinity, 1 << _usingElement.UsingElement))
                {
                   
                    if (hitInfo.collider.tag == "InteractionElement")
                    {
                      
                        // Check how far the interacted object is
                        _distanceWithObject = Vector3.Distance(hitInfo.collider.transform.position, RaycastOrigin.position);
                        // Send _grabposition and distance information to the object we interacted
                        Debug.Log(hitInfo.collider.name);
                        hitInfo.collider.GetComponent<ElementScript>().ElementMechanism(_grabPosition, _distanceWithObject);
                        //Debug.Log(_distanceWithObject);
                    }
                }
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {

        if (_miFirstPersonController.enabled == false)  // En caso de que hayamos saltado
        {
            _miFirstPersonController.enabled = true;
            _miCharacterController.enabled = true;
            miRigidBody.isKinematic = true;

            _miCapsuleCollider.enabled = false;
        }
    }

}