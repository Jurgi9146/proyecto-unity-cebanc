﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirePuzzle : ElementScript
{
    public bool _fireOn;
    public FirePuzzle[] _adjacentFires;
    private bool _firstRaycast = true;

    //Audio settings
    private AudioSource _fireAudioSource;
    public bool playingAudio = false;
    internal object startColor;

    private void Start()
    {
        _fireAudioSource = GetComponent<AudioSource>();
    } 
    protected override void InternalUpdate()
    {
        if(_firstRaycast == true)
        {
            BrazierOn();
            AdjacentBrazierOn();
            StartCoroutine(AllowSecondRaycast());
            _firstRaycast = false;
        }     
    }
    void BrazierOn()
    {
        if (_fireOn == true) //if fire is on
        {
            foreach (Transform child in transform) //set children game object to inactive
            {
                child.gameObject.SetActive(false);
                _fireOn = false; //variable initialization to new state
                
                if (playingAudio == false) //play audio once
                {
                    StartCoroutine(AudioPlay());
                    playingAudio = true;
                }
            }
        }
        else
        {
            foreach (Transform child in transform) //set childred game objects to active
            {
                child.gameObject.SetActive(true);
                _fireOn = true; //variable initialization to new state

                if (playingAudio == false) //play audio once
                {
                    StartCoroutine(AudioPlay());
                    playingAudio = true;
                }
            }
        }
    }

    //make adjacent brazierz change their state based on their previous state 
    void AdjacentBrazierOn()
    {
        for (int i = 0; i < _adjacentFires.Length; i++)
        {
            if (_adjacentFires[i]._fireOn == true) //if braziers were ON
            {
                foreach (Transform child in _adjacentFires[i].transform) //turn them off
                {
                    child.gameObject.SetActive(false);
                    _adjacentFires[i]._fireOn = false;
                }
            }
            else //if braziers were off
            {
                foreach (Transform child in _adjacentFires[i].transform)  //turn the on
                {
                    child.gameObject.SetActive(true);
                    _adjacentFires[i]._fireOn = true;
                }
            }
        }
    }

    //allow a second raycast after 0.5 second. 
    IEnumerator AllowSecondRaycast()
    {
        yield return new WaitForSeconds(0.5f);
        _activatedMechanism = false;
        _firstRaycast = true;
    }

    //Play audio once
    IEnumerator AudioPlay()
    {
        _fireAudioSource.Play();
        playingAudio = false;

        yield return null;
    }
}
