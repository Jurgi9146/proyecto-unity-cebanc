﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleGeneralScript : MonoBehaviour
{
    private FirePuzzle[] _PuzzleGeneral;
    private bool[] _initialBrazierStateArray;
    private bool _endGame;
    [Header ("Door to be open")]    
    public Transform _door;
    private Vector3 _DoorOpen;
    private Quaternion _DoorOpenQuaternion;
    public GameObject _torch;
    private bool _firstRoutine = true;
    public AudioSource _DoorAudioSource;

    public Color _puzzleFinishedFireColor;

    private void Start()
    {
        _PuzzleGeneral = GetComponentsInChildren<FirePuzzle>();
        _DoorOpen = new Vector3(_door.rotation.eulerAngles.x, _door.rotation.eulerAngles.y + 85, _door.rotation.eulerAngles.z); //set final door/floorplate final state
        _DoorOpenQuaternion = Quaternion.Euler(_DoorOpen);
        _initialBrazierStateArray = new bool[_PuzzleGeneral.Length]; //save initial brazier states for game reset
        //Save initial brazier's state
        for (int i =0; i< _PuzzleGeneral.Length; i++)
        {
            _initialBrazierStateArray[i] = _PuzzleGeneral[i]._fireOn;
        }

        ResetTorchScript.OnResetPuzzle += ResetAntorchScript_OnResetPuzzle; //Add-on the event
    }

    private void ResetAntorchScript_OnResetPuzzle()
    {
        ResetPuzzle(); //Reset puzzle after antorch
    }

    private void Update()
    {
        CheckPuzzleEnded(); //chek if the puzzle is already ended
        if (_endGame == true) //if puzzle is ended
        {
            OpenDoor(); //open the door. 

            // Changes the fire color when the puzzle gets completed
            for (int i = 0; i < _PuzzleGeneral.Length; i++)
            {
                foreach (Transform child in _PuzzleGeneral[i].GetComponentsInChildren<Transform>())
                {
                    foreach (Transform grandchild in child)
                    {
                        ParticleSystem pSystem = grandchild.GetComponent<ParticleSystem>();
                        if (pSystem != null)
                        {
                            var particles = pSystem.main;
                            particles.startColor = _puzzleFinishedFireColor;
                        }
                    }
                }
            }
        }
    }

    //Check if the puzzle is ended
    void CheckPuzzleEnded() 
    {
        _endGame = true; //by default the game is ended
        for (int i=0; i< _PuzzleGeneral.Length; i++) //check all brazier's state
        {
            if (_PuzzleGeneral[i]._fireOn != true) //if one of the brazier is not already ON. Change endgame variable
            {
                _endGame = false; 
            }
        }
    }

    void OpenDoor()
    {
        if (_firstRoutine == true)
        {
            StartCoroutine(PlayAudio()); //Play audio once
            _firstRoutine = false;
        }
        _door.rotation = Quaternion.RotateTowards(_door.rotation, _DoorOpenQuaternion, 50 * Time.deltaTime);
    }

    //Reset the puzzle if the torch is pressed
    void ResetPuzzle()
    {
        for (int i=0; i<_PuzzleGeneral.Length; i++)
        {
            _PuzzleGeneral[i]._fireOn = _initialBrazierStateArray[i]; //Reser fireOn variable to initial state
            _PuzzleGeneral[i].playingAudio = false; //reset audio
                //Reset to initial active state.
            if (_PuzzleGeneral[i]._fireOn == true)
            {
                foreach (Transform child in _PuzzleGeneral[i].GetComponentsInChildren<Transform>())
                {
                    foreach (Transform grandchild in child)
                    {                        
                        grandchild.gameObject.SetActive(true);

                       /* foreach (Transform grandgrandchild in grandchild)
                        {
                            Debug.Log(grandgrandchild);
                            grandgrandchild.gameObject.SetActive(true);
                        }*/
                    }
                    
                   /* if (_PuzzleGeneral[i].GetInstanceID() != child.GetInstanceID())
                    {
                        child.gameObject.SetActive(true);
                    }*/
                }                   
            }
            else //reset to initial inactive states
            {
                foreach (Transform child in _PuzzleGeneral[i].GetComponentsInChildren<Transform>())
                {
                    foreach (Transform grandchild in child)
                    {
                        grandchild.gameObject.SetActive(false);
                    }
                   
                    /*if (_PuzzleGeneral[i].GetInstanceID() != child.GetInstanceID())
                    {
                       
                    }*/
                }
                
            }
        }
    }

    IEnumerator PlayAudio() //Play the audio
    {
        _DoorAudioSource.Play();
        yield return null;
    }
}
