﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrazierOn : ElementScript {
    public GameObject FireFlame;
    public Transform _door;
    private Vector3 _DoorOpen;
    private Quaternion _DoorOpenQuaternion;
    private AudioSource _audioFire;
    public int _RotationAngle;
    private bool FireON;

    private bool playingAudio = false;

    private AudioSource _fireAudioSource;
    public AudioSource _doorAudioSource;

    private void Start()
    {
        _audioFire = GetComponent<AudioSource>();
        _DoorOpen = new Vector3(_door.rotation.eulerAngles.x, _door.rotation.eulerAngles.y - _RotationAngle, _door.rotation.eulerAngles.z);
        _DoorOpenQuaternion = Quaternion.Euler(_DoorOpen);

        _fireAudioSource = GetComponent<AudioSource>();
    }
    protected override void InternalUpdate()
    {
        FireFlame.SetActive(true);
        OpenDoor();

        if (playingAudio == false)
        {
            StartCoroutine(AudioPlay());
            playingAudio = true;
        }
    }

    void OpenDoor()
    {
        _door.rotation = Quaternion.RotateTowards(_door.rotation, _DoorOpenQuaternion, 50 * Time.deltaTime);

    }

    IEnumerator AudioPlay()
    {
        _fireAudioSource.Play();
        _doorAudioSource.Play();

        yield return null;
    }



}
