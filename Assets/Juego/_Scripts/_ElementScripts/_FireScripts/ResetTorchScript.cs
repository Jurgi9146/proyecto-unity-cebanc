﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetTorchScript : ElementScript {
    public delegate void ResetPuzzle();
    public static event ResetPuzzle OnResetPuzzle;

    private void Awake()
    {
        OnResetPuzzle = null;
    }

    protected override void InternalUpdate()
    {
        if (OnResetPuzzle !=null)
        {
            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(false);
            }
            _activatedMechanism = false;
            StartCoroutine(WaitForTorchTurnOn());
            OnResetPuzzle(); //Sen the reset event
        }
                    
    }

    IEnumerator WaitForTorchTurnOn()
    {
        yield return new WaitForSeconds(5f);
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(true);
        }
    }

}
