﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrazierOnDoorOpenClose : ElementScript {
    public GameObject FireFlame;
    public Transform _door;
    private Vector3 _DoorOpen;
    private Quaternion _DoorOpenQuaternion;
    private Quaternion _DoorInitialQuaternion;
    public int _RotationAngle;
    
    //Audio CONFIGURATION
    private bool _playingAudio = false;
    private AudioSource AudioFire;
    public AudioSource AudioDoor;

    private void Start()
    {
        AudioFire = GetComponent<AudioSource>();
        _DoorOpen = new Vector3(_door.rotation.eulerAngles.x, _door.rotation.eulerAngles.y - _RotationAngle, _door.rotation.eulerAngles.z);
        _DoorOpenQuaternion = Quaternion.Euler(_DoorOpen);
        _DoorInitialQuaternion = _door.rotation;           
    }
    protected override void InternalUpdate()
    {
            FireFlame.SetActive(true);
            OpenDoor();

        if (_playingAudio == false)
        {
            StartCoroutine(PlayAudio());
            _playingAudio = true;
        }
    }

    void OpenDoor()
    {
        _door.rotation = Quaternion.RotateTowards(_door.rotation, _DoorOpenQuaternion, 50 * Time.deltaTime);
        StartCoroutine(DelayTime());
    }
    void CloseDoor()
    {
        _door.rotation = Quaternion.RotateTowards(_door.rotation, _DoorInitialQuaternion, 50 * Time.deltaTime);
        FireFlame.SetActive(false);
        _activatedMechanism = false;
    }

    IEnumerator DelayTime()
    {
        yield return new WaitForSeconds(4f);
        CloseDoor();
    }

    IEnumerator PlayAudio()
    {
        AudioFire.Play();
        AudioDoor.Play();
        yield return null;
    }


}
