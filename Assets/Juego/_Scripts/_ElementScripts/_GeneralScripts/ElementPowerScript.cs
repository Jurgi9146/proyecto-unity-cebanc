﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ElementPowerScript : MonoBehaviour {
    //Declare transform variables
    [Header("FPS's transfrom variable")]
    public Transform _FPCTransform;
    private Transform _ElementPowerTransform;
    public GameObject _ElementPowerText;
    private bool _elemetObtained;
    private bool _firstRoutine = true;
    private AudioSource _elementAudioSource;

    //Call the event
    public delegate void ObtainedElement(bool elementObtained);
    public event ObtainedElement OnObtainedElement;

    private void Awake()
    {
        OnObtainedElement = null;
    }

    // Use this for initialization
    void Start ()
    {
        //Get components
        _elementAudioSource = GetComponent<AudioSource>();
        _ElementPowerTransform = GetComponent<Transform>();       
	}
	
	// Update is called once per frame
	void Update ()
    {
        NearElementPower();
	}

    void NearElementPower()

    {
      
        if (Vector3.Distance(_ElementPowerTransform.position, _FPCTransform.position) <= 3f) //if FPC is closer than 3 from the power
        { 
            //Show message in canvas
            if (_elemetObtained == false)
            {
                _ElementPowerText.SetActive(true);
            }
            
            if (Input.GetKeyDown(KeyCode.E)) //When the player prese's E
            {
                _ElementPowerText.SetActive(false); //remove message from canvas
                _elemetObtained = true;      //variable acutalization
                //Call the event
                if (OnObtainedElement !=null)
                {
                    OnObtainedElement(_elemetObtained);
                }
                                                             
                StartCoroutine(PlayAudio()); //start audio playing corouitne
            }
        }
        else
        {
            _ElementPowerText.SetActive(false); 
        }
    }

    //Play audio once
   IEnumerator PlayAudio()
    {
        _elementAudioSource.Play();
        yield return new WaitForSeconds(0.3f);
        Destroy(gameObject); //destroy the object after 0.5sec.
    }
}
