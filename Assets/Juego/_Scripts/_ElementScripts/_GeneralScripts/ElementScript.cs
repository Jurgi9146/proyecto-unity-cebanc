﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ElementScript : MonoBehaviour {

    public bool _activatedMechanism;
    protected Transform _transform;
    protected float _distance;

    private void Update()
    {
        if (_activatedMechanism == true)
        {
            InternalUpdate();
        }
    }

    protected abstract void InternalUpdate();

    public virtual void ElementMechanism(Transform transform, float distance)
    {
        _activatedMechanism = true;
        _transform = transform;
        _distance = distance;
    }
}
