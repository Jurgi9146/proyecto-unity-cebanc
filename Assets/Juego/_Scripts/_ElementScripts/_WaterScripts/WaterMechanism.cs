﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterMechanism : ElementScript
{
    //Variable Initialization
    public GameObject _propulsionPlatform;
    public GameObject _jarWater;
    private Color _platformColor;
    private float counter;
    private float TotalFillWaterTime = 2f;
    private bool _fistRoutine = true;
    private AudioSource _vaseAudioSource;

    private void Start()
    {
        _vaseAudioSource = GetComponent<AudioSource>();
    }
    protected override void InternalUpdate()
    {
        WaterFillUp();
    }

    void WaterFillUp()
    {
        //Start audio
        if (_fistRoutine == true)
        {
            StartCoroutine(PlayAudio());
            _fistRoutine = false;
        }
        counter += Time.deltaTime;
        if (counter > TotalFillWaterTime)
        {
            //Show vase filled up
            _jarWater.SetActive(true);
            //Activate plataform
            _propulsionPlatform.GetComponent<Collider>().enabled = true;
            MeshRenderer renderer = _propulsionPlatform.GetComponent<MeshRenderer>();
            Color c = renderer.material.color;
            c.a = 1.0f;
            renderer.material.color = c;
            //stop audio
            StopCoroutine(PlayAudio());
            _vaseAudioSource.Stop();
        }

    }
    IEnumerator PlayAudio()
    {
        _vaseAudioSource.Play();
        yield return null;
    }

}
