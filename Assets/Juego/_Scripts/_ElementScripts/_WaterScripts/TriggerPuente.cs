﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerPuente : MonoBehaviour
{

    public GameObject[] hiddenBridge;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "InteractionElement")
        {
            for (int i = 0; i < hiddenBridge.Length; i++)
            {
                hiddenBridge[i].SetActive(true);
            }
        }
    }
}
