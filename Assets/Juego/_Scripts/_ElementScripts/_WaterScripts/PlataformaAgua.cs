﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PlataformaAgua : MonoBehaviour
{

    public Transform _impulsePosition;

    private CharacterController _miCharacterController;
    private FirstPersonController _miFirstPersonController;
    private CapsuleCollider _miCapsuleCollider;

    private Rigidbody miRigidBody;

    //Audio settings
    private AudioSource _platformAudioSource;


    public float _jumpForce;

    private void Start()
    {
        _platformAudioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            miRigidBody = other.GetComponent<Rigidbody>();

            _miFirstPersonController = other.GetComponent<FirstPersonController>();
            _miCharacterController = other.GetComponent<CharacterController>();
            _miCapsuleCollider = other.GetComponent<CapsuleCollider>();

            ImpulseAction();
        }
    }

    private void ImpulseAction()
    {
        miRigidBody.angularVelocity = Vector3.zero;
        miRigidBody.velocity = Vector3.zero;


        // Desactivamos los componentes necesarios para salir impulsados
        _miFirstPersonController.enabled = false;
        _miCharacterController.enabled = false;
        miRigidBody.isKinematic = false;

        _miCapsuleCollider.enabled = true;

        miRigidBody.AddForce(_jumpForce * _impulsePosition.forward, ForceMode.Impulse);
        _platformAudioSource.Play(); //Play the audio
    }
}
