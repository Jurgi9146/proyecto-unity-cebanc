﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthMechanismStairs : ElementScript
{

    //Earth variable initialization
    private Transform StairsTranform;
    public float StairDesplazamnet;
    private Vector3 StairFinalScale;
    private Vector3 StairsInitialScale;
    private Vector3 StairsOriginalScale;
    public float EarthTranslationTime;

    //AudioConfiguration
    private bool _firstRoutine = true;
    private AudioSource _stariAudioSource;



    private void Start()
    {
        _stariAudioSource = GetComponent<AudioSource>();
        StairsTranform = GetComponent<Transform>();
        //Stair initial scale values
        StairFinalScale = new Vector3(StairsTranform.localScale.x , StairsTranform.localScale.y + StairDesplazamnet, StairsTranform.localScale.z);
        StairsInitialScale = StairsTranform.localScale;
        StairsOriginalScale = StairsTranform.localScale;
    }
    protected override void InternalUpdate()
    {
        StairsMovement();
    }

    void StairsMovement()
    {
        //Play audio just once
        if (_firstRoutine == true)
        {
            StartCoroutine(PlayAudio());
            _firstRoutine = false;
        }
        //Stair movement
        StairsInitialScale = Vector3.MoveTowards(StairsInitialScale, StairFinalScale,  EarthTranslationTime * Time.deltaTime);
        StairsTranform.localScale = StairsInitialScale;
        if (StairsInitialScale == StairFinalScale) //When stair reaches the final scale
        {
            //Stop audio;
            StopCoroutine(PlayAudio());
            _stariAudioSource.Stop(); 
            //start stair fall corroutine
            StartCoroutine (CounterCoroutine());

        }
        
    }

    //counter for making stair fall
    IEnumerator CounterCoroutine()
    {       
        yield return new WaitForSeconds(8f);
        StairsFall();
        StairsTranform.localScale = StairsOriginalScale; //initialize stairs scale
        _activatedMechanism = false;
    }

    //make stair fall after counter
    void StairsFall()
    {
        StairsTranform.localScale =Vector3.MoveTowards(StairsInitialScale, StairFinalScale, 1 / EarthTranslationTime * Time.deltaTime);
        if (StairsTranform.localScale == StairFinalScale) //initialize stair original position after falling
        {
            StairsTranform.localScale = StairsOriginalScale;
            StairsInitialScale = StairsOriginalScale;
        }

    }

    //Play stair creation audio once
    IEnumerator PlayAudio()
    {
        _stariAudioSource.Play();
        _firstRoutine = true;
        yield return null;
    }

}



              