﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthMechanismFloor : ElementScript
{

    //Earth variable initialization
    [Header("Floor Height and Velocity Setting")]
    public float Scale;
    public float EarthTranslationTime;
    private Transform FloorTransform;
    
    
    private ElementScript ElementScript;
    private Vector3 EarthFloorFinalScale;
    private Vector3 EarthFloorInitialScale;
    private Vector3 EarthOriginalScale;
    private bool FloorUp = true;
    private bool FloorMoving;

    //Audio Configuration
    [Header("AudioSource when floor is growing")]
    private AudioSource _audioFloorGrowing;
    private bool _firstRoutineUp = true;
    private bool _firstRoutineDown = true;


    private void Start()
    {
        //get component values
        FloorTransform = GetComponent<Transform>();
        _audioFloorGrowing = GetComponent<AudioSource>();
        //Save earth floor initial scale
        EarthFloorInitialScale = FloorTransform.localScale;
        EarthFloorFinalScale = new Vector3(FloorTransform.localScale.x, FloorTransform.localScale.y + Scale, FloorTransform.localScale.z);  //earth floor final scale
        EarthOriginalScale = EarthFloorInitialScale;
    }

    protected override void InternalUpdate()
    {
         if (FloorUp == true)
        {
            //move earth up
            MoveEarthFloorUp();

            //play earth audio once. call coroutine
            if (_firstRoutineUp == true)
            {
                StartCoroutine(AudioPlay());
                _firstRoutineUp = false;
            }

        }
         else if (FloorUp ==false)
        {
            //move earth down
            MoveEarthFloorDown();

            //play earth audio. call coroutine
            if (_firstRoutineDown == true)
            {
                StartCoroutine(AudioPlay());
                _firstRoutineDown = false;
            }           
        }
    }

    void MoveEarthFloorUp()
    {
        
        EarthFloorInitialScale = Vector3.MoveTowards(EarthFloorInitialScale, EarthFloorFinalScale, EarthTranslationTime * Time.deltaTime); //move earth up
        FloorTransform.localScale = EarthFloorInitialScale;
        if (EarthFloorInitialScale == EarthFloorFinalScale) //when earth reaches final scale
        {
            //variablea actualization
            FloorUp = false;
            _activatedMechanism = false;
            //AudioConfiguration
            StopCoroutine(AudioPlay()); //stop coroutine
            _audioFloorGrowing.Stop(); //stopaudio
                       
        }      
    }

    void MoveEarthFloorDown()
    {
        EarthFloorInitialScale = Vector3.MoveTowards(EarthFloorInitialScale, EarthOriginalScale, EarthTranslationTime * Time.deltaTime); //move earth down
        FloorTransform.localScale = EarthFloorInitialScale;
        if (EarthFloorInitialScale == EarthOriginalScale) //when earth reaches final scale
        {
            //variable actualization
            _activatedMechanism = false;
            FloorUp = true;
            //AudioConfiguration
            StopCoroutine(AudioPlay()); //stop coroutine
            _audioFloorGrowing.Stop(); //stopaudio                       
        }
    } 
    
    //Play audio once.
    IEnumerator AudioPlay()
    {
        _audioFloorGrowing.Play();
        _firstRoutineDown = true;
        _firstRoutineUp = true;
        yield return null;
        
    }   
}