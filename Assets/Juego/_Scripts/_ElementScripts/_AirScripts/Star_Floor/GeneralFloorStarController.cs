﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralFloorStarController : MonoBehaviour {

    //CAMBIAR
    public  GameObject[] _starArray;
    public GameObject _middlePiece;
    private bool _allowMidlePieceDown;
    private Vector3 _middlePieceInitialScale;
    private Vector3 _middlePieceFinalScale;

	// Use this for initialization
	void Start ()
    {
        _middlePieceFinalScale = new Vector3(1f, 1f, 1f);
	}
	
	// Update is called once per frame
	void Update ()
    {
        FinalPuzzle();
        if (_allowMidlePieceDown == true)
        {
            MiddlePieceDown();
        }
	}

    void FinalPuzzle() //Allow middle piece down if all start edges are already down
    {
        _allowMidlePieceDown = true;

        for (int i =0; i<_starArray.Length; i++)
        {
            if (_starArray[i].transform.localScale !=_middlePieceFinalScale)
            {
                _allowMidlePieceDown = false;
            }
        }       

    }
    void MiddlePieceDown()
    {
        _middlePieceInitialScale = Vector3.MoveTowards(_middlePieceInitialScale, _middlePieceFinalScale, 10 * Time.deltaTime);
        _middlePiece.transform.localScale = _middlePieceInitialScale;
    }
}
