﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorStarControllerEdge4 : MonoBehaviour {

    public GameObject _cageMask;
    public GameObject _Antorch;
    public Rigidbody _mask;
    private Vector3 cageInitialScale;
    private Vector3 cageFinalScale;
    private bool _FirstTimeAntorchSet = true;

    // Use this for initialization
    void Start () {
        cageInitialScale = _cageMask.transform.localScale;
        cageFinalScale = new Vector3(1.5f, 0f, 1.5f);
    }
	
	// Update is called once per frame
	void Update ()
    {
        //Check if Floor is already down
        if (transform.localScale == new Vector3(1f, 1f, 1f))
        {
            ActivateNextPuzzle();
        }
	}
    void ActivateNextPuzzle()
    {
        //Set antorch active. --> Avoid conflict with bust Trigger. 
        if (_FirstTimeAntorchSet == true)
        {
            _Antorch.SetActive(true);
            _FirstTimeAntorchSet = false;
        }

        //Remove mask cage
        cageInitialScale = Vector3.MoveTowards(cageInitialScale, cageFinalScale, 2 * Time.deltaTime);
        _cageMask.transform.localScale = cageInitialScale;

        if (cageInitialScale == cageFinalScale)
        {
            if (_mask !=null)
            {
                _mask.constraints = RigidbodyConstraints.None; //Unfreez rotation and position of the mask
            }           
        }                      
    }
}
