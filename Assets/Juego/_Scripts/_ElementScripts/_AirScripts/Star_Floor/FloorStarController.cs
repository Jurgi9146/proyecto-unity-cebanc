﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorStarController : MonoBehaviour {

    public GameObject _cageMask;
    public Rigidbody _mask;
    public GameObject _cageCaesar;
    private Vector3 cageInitialScale;
    private Vector3 cageFinalScale;
    private Vector3 cageCaesarInitialScale;
    private Vector3 cageCaesarFinalScale;



    // Use this for initialization
    void Start () {
        cageInitialScale = _cageMask.transform.localScale;
        cageFinalScale = new Vector3(1.5f, 0f, 1.5f);
        cageCaesarInitialScale = _cageCaesar.transform.localScale;

    }
	
	// Update is called once per frame
	void Update ()
    {
        //Check if Floor is already down
        if (transform.localScale == new Vector3(1f, 1f, 1f))
        {
            ActivateNextPuzzle();
        }

	}
    void ActivateNextPuzzle()
    {
        //Remove mask cage
        cageInitialScale = Vector3.MoveTowards(cageInitialScale, cageFinalScale, 2 * Time.deltaTime);
        _cageMask.transform.localScale = cageInitialScale;
        //Remove Caesar cage
        cageCaesarInitialScale = Vector3.MoveTowards(cageCaesarInitialScale, cageFinalScale, 2 * Time.deltaTime);
        _cageCaesar.transform.localScale = cageCaesarInitialScale;

        if (cageInitialScale == cageFinalScale)
        {
            if (_mask !=null)
            {
                _mask.constraints = RigidbodyConstraints.None; //Unfreez rotation and position of the mask
            }
            
        }                      
    }
}
