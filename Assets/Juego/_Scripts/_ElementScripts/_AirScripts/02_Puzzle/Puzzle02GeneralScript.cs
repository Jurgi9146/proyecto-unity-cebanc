﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle02GeneralScript : MonoBehaviour
{
    private FirePuzzle[] _PuzzleGeneral;
    private bool[] _initialBrazierStateArray;
    private bool _endGame;
    [Header("Stair to be Scale")]
    public Transform _stair;
    private Vector3 _finalScale;
    private Vector3 _initialScale;
    public GameObject _torch;
    private bool _firstRoutine = true;
    public AudioSource _StairAudioSource;
    public Color _puzzleFinishedFireColor;

    private void Start()
    {
        _PuzzleGeneral = GetComponentsInChildren<FirePuzzle>();
        _finalScale = new Vector3(3f, 60f, 1.5f);
        _initialScale = _stair.transform.localScale;
        _initialBrazierStateArray = new bool[_PuzzleGeneral.Length]; //save initial brazier states for game reset
      
        //Save initial brazier's state
        for (int i = 0; i < _PuzzleGeneral.Length; i++)
        {
            _initialBrazierStateArray[i] = _PuzzleGeneral[i]._fireOn;
        }

        ResetTorchScript.OnResetPuzzle += ResetAntorchScript_OnResetPuzzle; //Add-on the event
    }

    private void ResetAntorchScript_OnResetPuzzle()
    {
        ResetPuzzle(); //Reset puzzle after antorch
    }


    private void Update()
    {
        CheckPuzzleEnded();
        if (_endGame == true)
        {
            ScaleStair();

            // Changes the fire color when the puzzle gets completed
            for (int i = 0; i < _PuzzleGeneral.Length; i++)
            {
                foreach (Transform child in _PuzzleGeneral[i].GetComponentsInChildren<Transform>())
                {
                    foreach (Transform grandchild in child)
                    {
                        ParticleSystem pSystem = grandchild.GetComponent<ParticleSystem>();
                        if (pSystem != null)
                        {
                            var particles = pSystem.main;
                            particles.startColor = _puzzleFinishedFireColor;
                        }
                    }
                }
            }
        }
    }

    void CheckPuzzleEnded()
    {
        _endGame = true;
        for (int i = 0; i < _PuzzleGeneral.Length; i++)
        {
            if (_PuzzleGeneral[i]._fireOn != true)
            {
                _endGame = false;
            }
        }
    }

    void ScaleStair()
    {
        if (_firstRoutine == true)
        {
            StartCoroutine(PlayAudio()); //Play the audio
            _firstRoutine = false;
        }
        _initialScale = Vector3.MoveTowards(_initialScale, _finalScale, 10 * Time.deltaTime);
        _stair.transform.localScale = _initialScale;
    }

        //Reset the puzzle if the torch is pressed
        void ResetPuzzle()
    {
            for (int i = 0; i < _PuzzleGeneral.Length; i++)
            {
                _PuzzleGeneral[i]._fireOn = _initialBrazierStateArray[i]; //Reser fireOn variable to initial state
                                                                          //Reset to initial active state.
            _PuzzleGeneral[i].playingAudio = false; // reset audio
                if (_PuzzleGeneral[i]._fireOn == true)
                {
                    foreach (Transform child in _PuzzleGeneral[i].GetComponentsInChildren<Transform>())
                    {
                        foreach (Transform grandchild in child)
                        {
                            grandchild.gameObject.SetActive(true);

                            /* foreach (Transform grandgrandchild in grandchild)
                             {
                                 Debug.Log(grandgrandchild);
                                 grandgrandchild.gameObject.SetActive(true);
                             }*/
                        }

                        /* if (_PuzzleGeneral[i].GetInstanceID() != child.GetInstanceID())
                         {
                             child.gameObject.SetActive(true);
                         }*/
                    }
                }
                else //reset to initial inactive states
                {
                    foreach (Transform child in _PuzzleGeneral[i].GetComponentsInChildren<Transform>())
                    {
                        foreach (Transform grandchild in child)
                        {
                            grandchild.gameObject.SetActive(false);
                        }

                        /*if (_PuzzleGeneral[i].GetInstanceID() != child.GetInstanceID())
                        {

                        }*/
                    }

                }
            }
        
        }

        IEnumerator PlayAudio() //Play the audio
    {
            _StairAudioSource.Play();
            yield return null;        
    }

}