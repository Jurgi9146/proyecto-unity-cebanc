﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaskTrigger : MonoBehaviour {

    public GameObject _hiddenMask;

    public float _changeScale;
    public GameObject _scaleObject;
    [HideInInspector]
    public bool _allowScale;
    Vector3 _initialScale;
    Vector3 _finalScale;

    //Audio configuration
    public AudioSource _scaledObjectAudioSource;
    private bool _firstRoutine = true;

    private void Start()
    {
        _initialScale = _scaleObject.transform.localScale;
        _finalScale = new Vector3(1f, 1f, 1f);
    }
    private void Update()
    {
        if (_allowScale == true)
        {
            CompletedMask();
        }
    }

    private void OnTriggerEnter(Collider other) //When grabed mask enteres caesar bust trigger
    {
        if (other.gameObject.tag == "InteractionElement")
        {
            other.GetComponent<AirMechanismGrab>()._onMaskTrigger = true; //variable initialization
        }
    }

    private void OnTriggerExit(Collider other) //when grabed mask exists caesar bust trigger
    {
        if (other.gameObject.tag == "InteractionElement")
        {
            other.GetComponent<AirMechanismGrab>()._onMaskTrigger = false; //variable initialization
        }
    }

    //Make an object scale down. Earth floor, cage.
    void CompletedMask()
    {
        //Play audio
        if (_firstRoutine == true)
        {
            StartCoroutine(PlayAudio());
            _firstRoutine = false;
        }

        //Make floor star edge scale down
        _initialScale = Vector3.MoveTowards(_initialScale, _finalScale, 10*Time.deltaTime);
        _scaleObject.transform.localScale = _initialScale;
    }

    //play audio
    IEnumerator PlayAudio()
    {
        _scaledObjectAudioSource.Play();
        yield return null;
    }
}
