﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirPuzzleGeneralScript : MonoBehaviour {
    private int _selectedOrder = 1;
    private bool _allowSum = true;
    public AirPuzzleScript[] _selectedFloorPlates;
    public GameObject[] _starFormation;
    public float _scale;

    public delegate void FinalStairsActivation();
    public static event FinalStairsActivation OnFinalStairsActivation;

    //Audio settings
    private AudioSource[] _selectedFloorPlateAudioSource;
    private bool _firstRoutine = true;

    private void Awake()
    {
        OnFinalStairsActivation = null;
    }


    private void Start()
    { 
        //Add-on the event
        AirPuzzleScript.OnActivateFloorplate += AirPuzzleScript_OnActivateFloorplate;
        _selectedFloorPlateAudioSource = GetComponentsInChildren<AudioSource>(); //get all audio sources
    }

    private void AirPuzzleScript_OnActivateFloorplate(int order)
    {
       if (_selectedOrder == order)//If the platform we are raycasting with is the desired one (the next one on the desired order)
        {
            _selectedFloorPlates[_selectedOrder -1].transform.localScale = new Vector3(1f, _scale, 1f);//Allow scale translation. (Make the selected floorplate go up)
            _allowSum = true;
            //Play audio
            if (_firstRoutine ==true)
            {
                StartCoroutine(PlayAudio());
                _firstRoutine = false;
            }
            if (_selectedOrder >=2) //when two platforms are up, set particle system on
            {
                _starFormation[_selectedOrder-2].SetActive(true);
            }
        }
       else if (_selectedOrder != order) //if the selected order is incorrect, make platforms go down, set inactive particle system
        {
            for (int i=0; i< _selectedFloorPlates.Length; i++)
            {
                _selectedFloorPlates[i].transform.localScale = new Vector3(1f, 1f, 1f);
                _starFormation[i].SetActive(false);
            }
            _allowSum = false;
            _selectedOrder = 1;
        }
       if (_selectedOrder == _selectedFloorPlates.Length) //End the game after making all the desired platforms go up
        {
            FinishGame();
        }
       if (_allowSum == true)
        {
            _selectedOrder++;
        }
    }

    //Finish the game
    void FinishGame()
    {
        _starFormation[_selectedOrder-1].SetActive(true);
        //Finalizar Juego
        if (OnFinalStairsActivation !=null)
        {
            OnFinalStairsActivation(); //call the stairs event. Make final stairs appear
        }
    }

    IEnumerator PlayAudio()
    {
        _selectedFloorPlateAudioSource[_selectedOrder - 1].Play();
        yield return new WaitForSeconds(0.2f);
        _firstRoutine = true;
    }

}
