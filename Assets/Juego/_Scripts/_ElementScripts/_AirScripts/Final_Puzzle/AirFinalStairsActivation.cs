﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirFinalStairsActivation : MonoBehaviour {

    public int _stairsScale;
    private bool _allowScale;
    private Vector3 FinalScale;

    //Audio setting
    private AudioSource _finalStairsAudioSource;
    private bool _firstCoroutine = true;

    // Use this for initialization
    void Start()
    {
        _finalStairsAudioSource = GetComponent<AudioSource>();
        FinalScale = new Vector3(transform.localScale.x, _stairsScale, transform.localScale.z); //select floor plates final scale
        AirPuzzleGeneralScript.OnFinalStairsActivation += AirPuzzleGeneralScript_OnFinalStairsActivation; //Add on the final puzzle event
    }

    private void AirPuzzleGeneralScript_OnFinalStairsActivation()
    {
        _allowScale = true; //when the puzzle is finished, allow stair scale
    }

    private void Update()
    {
        if (_allowScale == true)
        {
            transform.localScale = Vector3.MoveTowards(transform.localScale, FinalScale, Time.deltaTime *10);

            //Play Audio once
            if (_firstCoroutine == true)
            {
                StartCoroutine(PlayAudio());
                _firstCoroutine = false;
            }
        }


    }

    IEnumerator PlayAudio()
    {
        _finalStairsAudioSource.Play();
        yield return null;
    }
}
