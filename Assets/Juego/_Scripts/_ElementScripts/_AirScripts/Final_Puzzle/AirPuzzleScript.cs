﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirPuzzleScript : ElementScript {

    public int _selectedOrder;
    private bool _sendEvent = true;
    private bool _firstRaycast = true;
    public delegate void ActivateFloorPlate(int order);
    public static event ActivateFloorPlate OnActivateFloorplate;



    private void Awake()
    {
        OnActivateFloorplate = null; //Add-on the event
    }


    protected override void InternalUpdate()
    {
        //Allow one raycast ever 0.5sec
        if (_firstRaycast == true)
        {
            SendTheEvent();
            StartCoroutine(AllowSecondRaycast());
            _firstRaycast = false;
        }

    }
    void SendTheEvent()
    {
        //sen the event after raycast collides with floor plate
        if (OnActivateFloorplate != null)
        {
            OnActivateFloorplate(_selectedOrder);
        }
    }

    //timer
    IEnumerator AllowSecondRaycast()
    {
        yield return new WaitForSeconds(0.5f);
        _activatedMechanism = false;
        _firstRaycast = true;
    }
}
