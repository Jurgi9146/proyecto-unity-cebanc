﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ChangeBrazierLayer : MonoBehaviour {

    public GameObject _brazier;
    private Transform _FloorTransform;
	// Use this for initialization
	void Start () {
        _FloorTransform = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        if (_FloorTransform.localScale == new Vector3(1f, 1f, 1f))
        {
            _brazier.layer = 9; //Change brazier to fire layer
        }
		
	}
}
