﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrazierGeneralScript : MonoBehaviour {

    public BrazierScript[] _Braziers;
    private bool _allBraziersOn;
    private int _position;
    private bool _allowDoorClose = true;

    private void Start()
    {
        BrazierScript.OnBrazierOn += BrazierScript_OnBrazierOn; //Add-on the event
    }

    private void BrazierScript_OnBrazierOn(int position)
    {       
        _position = position; //save the position of the brazier turned on
    }

    // Update is called once per frame
    void Update ()
    {
        CheckAllBraziers(); //Check if all braziers are turned on in correct order
        if (_allBraziersOn == true)  //if all braziers are on
        {
            OpenDoor(); //opend oor
        }		
	}

    void CheckAllBraziers()
    {
        _allBraziersOn = true; //all braziers are on by default
        for (int i = 0; i < _Braziers.Length; i++) //if one of them is not on, change variable
        {
            if (_Braziers[i]._FlameOn != true)
            {
                _allBraziersOn = false;
            }
        }

        if (_allBraziersOn == true)
        {
            for (int i =0; i<_Braziers.Length; i++)
            {
                _Braziers[i].gameObject.tag = "Untagged";
            }
        }
    }

   void OpenDoor()
    {        
        _Braziers[_position]._AllowOpenDoor = true; //Allow door open
        if(_position !=2 && _allowDoorClose == true) //If braziers are not turn On in the desired order
        {
            StartCoroutine(ResetPuzzle()); //Reset puzzle           
        }
       
    }

    //Reset the puzzle
    IEnumerator ResetPuzzle()
    {
        yield return new WaitForSeconds(2f);
        for (int i=0; i<_Braziers.Length; i++)
        {
            _Braziers[i]._FlameOn = false; //Set bool variable to false
            _Braziers[i].FireFlame.SetActive(false); //Set braziers fireflame to false
            _Braziers[i]._AllowOpenDoor = false; //Set open door bool variable to false
            _Braziers[i]._door.rotation = _Braziers[i]._doorInitialRotation; //Set doors to original position
            _Braziers[i].gameObject.tag = "InteractionElement";
            _allBraziersOn = false;
        }

    }

    //Make sure door's are not close with the player insde the room. 
    private void OnTriggerEnter(Collider other)
    {
        _allowDoorClose = false;
    }

    private void OnTriggerExit(Collider other)
    {
        _allowDoorClose = true;
    }
}
