﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrazierScript : ElementScript {

    public GameObject FireFlame;
    public int _position;
    public bool _FlameOn;
    public bool _AllowOpenDoor;
    public Transform _door;
    [HideInInspector]
    public Quaternion _doorInitialRotation;
    public int _RotationAngle;

    public delegate void BrazierOn(int position);
    public static event BrazierOn OnBrazierOn;

    //Audio settings
    private AudioSource _brazierAudioSource;
    public AudioSource _doorAudioSource;
    private bool _firstBrazierRoutine = true;
    private bool _firstDoorRoutine = true;

    private void Awake()
    {
        OnBrazierOn = null;
    }
    // Use this for initialization
    private void Start()
    {
        _brazierAudioSource = GetComponent<AudioSource>();
        _doorInitialRotation = _door.rotation; //Save door initial rotation
    }

    // Update is called once per frame
    protected override void InternalUpdate()
    {
        FireFlame.SetActive(true); //set brazier flame on
        _activatedMechanism = false; // allow second raycast
        _FlameOn = true;
        //Play brazier audio once.
        if (_firstBrazierRoutine ==true)
        {
            StartCoroutine(PlayBrazierAudio());
            _firstBrazierRoutine = false;
        }

        if (_AllowOpenDoor == true) //if puzzle is finished in the correc order
        {            
            OpenDoor(); //Open door
        }
        if (OnBrazierOn !=null) //Call the event
        {
            OnBrazierOn(_position);
        }
    }

    void OpenDoor()
    {
        _door.rotation = Quaternion.Euler(_door.rotation.x, _RotationAngle, _door.rotation.z); //Open the door
        //Play open door audio once
        if (_firstDoorRoutine ==true)
        {
            StartCoroutine(PlayDoorAudio());
            _firstDoorRoutine = false;
        }
        
    }

    //Brazier Audio
    IEnumerator PlayBrazierAudio()
    {
        _brazierAudioSource.Play();
        yield return null;
    }
    //Door Audio
    IEnumerator PlayDoorAudio()
    {
        _doorAudioSource.Play();
        yield return null;
    }

}
