﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirMechanismGrab : ElementScript
{
    
    [Header("Air Mechanics Settings")]

    public int throwForce;
    private Rigidbody _objectRigidBody;
    private Transform _objectTransform;
    private static bool _firstTime = true;

    private bool _objetoCogido = false;

    public delegate void FirstInformationEvent();
    public static event FirstInformationEvent OnFirstInfoEvent;

    //Audio configuration
    private AudioSource _objectAudioSource;
    public AudioClip _objectGrab;
    public AudioClip _objectThrow;
    private bool _firstRoutine = true;

 
    public bool _onMaskTrigger = false;
    public MaskTrigger _maskTrigger;
  //  public delegate void MaskTriggerEvent();
  //  public static event MaskTriggerEvent OnMaskTriggerEvent;

    private void Awake()
    {
        OnFirstInfoEvent = null;
      //  OnMaskTriggerEvent = null;
    }

    private void Start()
    {
        _objectAudioSource = GetComponent<AudioSource>();
        _objectRigidBody = GetComponent<Rigidbody>();
        _objectTransform = GetComponent<Transform>();
    }

    protected override void InternalUpdate()
    {
        if (_distance <= 30)
        {
            if (!_objetoCogido)
            {
                _objectRigidBody.useGravity = false;
                //heldObject.GetComponent<Collider>().enabled = false;

                // Atraemos el objeto hacia nosotros
                _objectTransform.position = Vector3.MoveTowards(_objectTransform.position, _transform.position, 10 * Time.deltaTime);

                //Audio configuration
                if (_firstRoutine == true)
                {
                    StartCoroutine(PlayGrabAudio());
                    _firstRoutine = false;
                }

                // Si el objeto ha llegado hasta nosotros, lo sujetamos
                //Mandamos la primera vez un canvas de informacion del raton
                if (_objectTransform.position == _transform.position)
                {
                    _objetoCogido = true;
                    if (_firstTime == true)
                    {
                        if (OnFirstInfoEvent !=null)
                        {
                            OnFirstInfoEvent(); //Lanzar el evento para el canvas
                        }
                        _firstTime = false;
                    }
                }
                // Activamos la gravedad en caso de que no hayamos cogido el objeto
                else if (Input.GetKeyUp(KeyCode.E) && _objectTransform.position != _transform.position)
                {
                    _objectRigidBody.useGravity = true;
                    _activatedMechanism = false;

                    //Audio configuration
                    StopCoroutine(PlayGrabAudio());
                    _objectAudioSource.Stop();    ///CAMBIARRRRR!!!!!******************
                    _firstRoutine = true;
                }
            }
            else    // Si hemos cogido el objeto
            {
                // Lo fijamos a la posición correspondiente
                _objectTransform.position = _transform.position;
                _objectTransform.rotation = _transform.rotation;
                gameObject.layer = 12; //Change layer to selectedElement

                //Audio configuration. Grab
                StopCoroutine(PlayGrabAudio());
                _objectAudioSource.Stop();
                _firstRoutine = true;

                if (Input.GetButtonDown("Fire1"))       // LANZAR OBJETO
                {
                    //Audio configuration Throw !
                    _objectAudioSource.PlayOneShot(_objectThrow);
                  
                    gameObject.layer = 10; //Change layer to Air Layer
                    _objectRigidBody.useGravity = true;
                    //heldObject.GetComponent<Collider>().enabled = true;

                    _objectRigidBody.AddForce(throwForce * _transform.forward, ForceMode.Impulse);
                    //heldObject = null;

                    _objetoCogido = false;
                    _activatedMechanism = false;                   
                }

                if (Input.GetButtonDown("Fire2") && _onMaskTrigger == false)       // SOLTAR OBJETO
                {
                    gameObject.layer = 10; //Change layer to Air Layer
                    
                    _objectRigidBody.useGravity = true;

                    _objectRigidBody.AddForce(0 * _transform.forward);

                    _objetoCogido = false;
                    _activatedMechanism = false;
                }
                if (Input.GetButtonDown("Fire2") && _onMaskTrigger == true)
                {
                    gameObject.layer = 10; //Change layer to Air Layer
                    
                    _maskTrigger._hiddenMask.SetActive(true);
                    _maskTrigger._allowScale = true;
                    _activatedMechanism = false;
                    Destroy(gameObject);
                }
            }
        }
    }


    IEnumerator PlayGrabAudio()
    {
        _objectAudioSource.PlayOneShot(_objectGrab);
        yield return null;
    }
    
}
