﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchScript : ElementScript
{
    [Header("Air Mechanics Settings")]

    public int throwForce;
    private Rigidbody _objectRigidBody;
    private Transform _objectTransform;
    private static bool _firstTime = true;

    private bool _objetoCogido = false;

    public AudioSource _cageAudioSource;

    //For caesar CAGE;
    public GameObject _caesarCage;
    public GameObject _caesarBust;
    public Rigidbody _caesarCageRB;

    private void Start()
    {
        _objectRigidBody = GetComponent<Rigidbody>();
        _objectTransform = GetComponent<Transform>();
    }

    protected override void InternalUpdate()
    {
        if (_distance <= 30)
        {
            if (!_objetoCogido)
            {
                _objectRigidBody.useGravity = false;

                // Atraemos el objeto hacia nosotros
                _objectTransform.position = Vector3.MoveTowards(_objectTransform.position, _transform.position + new Vector3(1.5f,0f,0f), 10 * Time.deltaTime);

                // Si el objeto ha llegado hasta nosotros, lo sujetamos
                //Mandamos la primera vez un canvas de informacion del raton
                if (_objectTransform.position == _transform.position + new Vector3(1.5f, 0f, 0f))
                {
                    _objetoCogido = true;
                }
                // Activamos la gravedad en caso de que no hayamos cogido el objeto
                else if (Input.GetKeyUp(KeyCode.E) && _objectTransform.position != _transform.position)
                {
                    _objectRigidBody.useGravity = true;
                    _activatedMechanism = false;
                }
            }
            else            // Si hemos cogido el objeto
            {
                // Lo fijamos a la posición correspondiente
                _objectTransform.position = _transform.position + new Vector3(1.5f, 0f, 0f);
                _objectTransform.rotation = _transform.rotation;

                if (Input.GetButtonDown("Fire1"))       // LANZAR OBJETO
                {
                    _objectRigidBody.useGravity = true;

                    _objectRigidBody.AddForce(throwForce * _transform.forward, ForceMode.Impulse);

                    _objetoCogido = false;
                    _activatedMechanism = false;
                }

                if (Input.GetButtonDown("Fire2"))       // SOLTAR OBJETO
                {
                    _objectRigidBody.useGravity = true;

                    _objectRigidBody.AddForce(0 * _transform.forward);

                    _objetoCogido = false;
                    _activatedMechanism = false;
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Chain")
        {
            Destroy(other.gameObject);
            _caesarCageRB.isKinematic = false;
            _cageAudioSource.Play(); //Play cage audio
            StartCoroutine(WaitForCollision());
        }
    }


    IEnumerator WaitForCollision()
    {
        yield return new WaitForSeconds(3f);
        _caesarBust.transform.parent = null; //Make caesar bust unchid   
        _caesarCage.transform.localScale = new Vector3(1.5f, 0f, 1.5f);
        Destroy(gameObject);
    }
}
