﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirPuzzle01CaesarFloorplate : MonoBehaviour {

    //Audio settings
    private AudioSource _floorPlateAudioSource;
    private bool _firstRoutine = true;
    private bool _floorPlateAlreadyUp;

	// Use this for initialization
	void Start ()
    {
        _floorPlateAudioSource = GetComponent<AudioSource>();
        AirPuzzle01GeneralScript.OnCaesarFloorPlateActivation += AirPuzzle01GeneralScript_OnCaesarFloorPlateActivation; //Add on the EVENT
	}

    private void AirPuzzle01GeneralScript_OnCaesarFloorPlateActivation(bool state)
    {
        if (state == true) //IF the floorplate selected order is correct
        {
            transform.localScale = new Vector3(1.5f, 1f, 1.5f); //make floorplate not hide caesar bust
            _floorPlateAlreadyUp = false;
        }

        else
        {
            transform.localScale = new Vector3(1.5f, 40f, 1.5f); //make floorplate hide caesar bust
            //Play audio coroutine
            if (_firstRoutine == true && _floorPlateAlreadyUp ==false)
            {
                StartCoroutine(PlayAudio());
                _firstRoutine = false;
                _floorPlateAlreadyUp = true;
            }
        }
    }

    //Audio corouitne
    IEnumerator PlayAudio()
    {
        _floorPlateAudioSource.Play(); //play audio
        yield return new WaitForSeconds(0.5f); 
        _firstRoutine = true; //Allow second audio
    }
}
