﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirPuzzle01Script : ElementScript {

    public int _selectedOrder;
    public int _scale;
    private bool _sendEvent = true;
    private bool _firstRaycast = true;
    public delegate void Activate01FloorPlate(int order);
    public static event Activate01FloorPlate OnActivate01Floorplate;


    private void Awake()
    {
        OnActivate01Floorplate = null; 
    }

    protected override void InternalUpdate()
    { 
        //Allow just one raycast every 0.5seconds.
        if (_firstRaycast == true)
        {
            SendTheEvent(); //Sen the event
            StartCoroutine(AllowSecondRaycast()); //Call delay coroutine
            _firstRaycast = false;
        }

    }
    void SendTheEvent()
    {
        if (OnActivate01Floorplate != null)
        {
            OnActivate01Floorplate(_selectedOrder); //Sen the event with the order number;
        }
    }
    IEnumerator AllowSecondRaycast()
    {
        yield return new WaitForSeconds(0.5f);
        _activatedMechanism = false; //Allow second raycast
        _firstRaycast = true;
    }
}
