﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirPuzzle01GeneralScript : MonoBehaviour {
    private int _selectedOrder = 1;
    private bool _allowSum = true;
    public AirPuzzle01Script[] _selectedFloorPlates;
    private bool _state;
     
    //Show caesar bust outside the earth platform
    public delegate void CaesarFloorPlateActivation(bool state);
    public static event CaesarFloorPlateActivation OnCaesarFloorPlateActivation;

    //Audio settings
    private AudioSource[] _selectedFloorPlatesAudioSource;
    private bool _firstRoutine = true;

    private void Awake()
    {
        OnCaesarFloorPlateActivation = null;
    }


    private void Start()
    {
        AirPuzzle01Script.OnActivate01Floorplate += AirPuzzle01Script_OnActivate01Floorplate; //Add-on the event
        _selectedFloorPlatesAudioSource = GetComponentsInChildren<AudioSource>();
    }

    private void AirPuzzle01Script_OnActivate01Floorplate(int order)
    {
       if (_selectedOrder == order) //If the platform we are raycasting with is the desired one (the next one on the desired order)
        {
            _selectedFloorPlates[_selectedOrder -1].transform.localScale = new Vector3(1.5f,60f + AirPuzzle01Script.FindObjectOfType<AirPuzzle01Script>()._scale , 1.5f); //Allow scale translation. (Make the selected floorplate go up)
            _allowSum = true;
            _state = true; //Caesar Floorplate down

            //Send caesar EVENT!! bool TRUE
            SendCaesarFloorPlateEvent(); //Make FloorPlate that hides Caesar bust scale down.

            if (_firstRoutine ==true)
            {
                StartCoroutine(PlayAudio()); //Play the audio Coroutine
                _firstRoutine = false;
            }
        }
       else if (_selectedOrder != order) //If the platform we are raycasting with is not the next one on the desired order
        {
            for (int i=0; i< _selectedFloorPlates.Length; i++) 
            {
                _selectedFloorPlates[i].transform.localScale = new Vector3(1.5f, 60f, 1.5f); //Make all floorplates scale down
            }
            _allowSum = false;
            _selectedOrder = 1; //Inicializar variable
            _state = false;
            //send Caesar event!! bool false
            SendCaesarFloorPlateEvent(); //Make Floorplate hide Caesarbust
        }

       if (_allowSum == true)
        {
            _selectedOrder++; //Initialize the selected order
        }
    }

    //send the event
void SendCaesarFloorPlateEvent()
    {
        if (OnCaesarFloorPlateActivation !=null)
        {
            OnCaesarFloorPlateActivation(_state);
        }
    }


    IEnumerator PlayAudio()
    {
        _selectedFloorPlatesAudioSource[_selectedOrder - 1].Play();
        yield return new WaitForSeconds(0.2f);
        _firstRoutine = true; 
    }
}
