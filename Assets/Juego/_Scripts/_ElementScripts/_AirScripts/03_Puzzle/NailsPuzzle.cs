﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NailsPuzzle : MonoBehaviour
{

    public int _selectedOrder;
    private bool _sendEvent = true;
    private bool _firstRaycast = true;
    public delegate void destroyNails(int order);
    public static event destroyNails OnDestroyNailsInOrder;

    private void Awake()
    {
        OnDestroyNailsInOrder = null;
    }


    void SendTheEvent()
    {
        if (OnDestroyNailsInOrder != null)
        {
            OnDestroyNailsInOrder(_selectedOrder);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "InteractionElement")
        {
            gameObject.SetActive(false);
            collision.gameObject.SetActive(false);
            SendTheEvent();
        }
    }
}
