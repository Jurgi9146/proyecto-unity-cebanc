﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NailsPuzzleGeneral : MonoBehaviour {
    public NailsPuzzle[] NailsInOrder;
    private int SelectedOrder = 1;
    public GameObject[] Vases;
    private bool _allowSum = true;
    Vector3[] InitPosition;
    private Vector3 ScaleA;
    private Vector3 ScaleB;
    public GameObject scaleobject;
    private bool Floorup;

    //audio settings
    public AudioSource _floorPlateAudioSource;
    private bool _firstRoutine = true;

    void Start () {
        
        ScaleA = scaleobject.transform.localScale;
        ScaleB = new Vector3(1f, 1f, 1f);
        NailsPuzzle.OnDestroyNailsInOrder += NailsPuzzle_OnDestroyNailsInOrder;
        InitPosition = new Vector3[Vases.Length];

        for (int i = 0; i < Vases.Length; i++)
        {
            InitPosition[i] = Vases[i].transform.position;
        }
	}

    private void NailsPuzzle_OnDestroyNailsInOrder(int order)
    {
        if (SelectedOrder == order) //Orden correcto
        {
           
            NailsInOrder[SelectedOrder - 1].gameObject.SetActive(false);
          
            _allowSum = true;
         
        }
        else if (SelectedOrder != order) //Orden incorrecto
        {
            for (int i = 0; i < NailsInOrder.Length; i++)
            {
                NailsInOrder[i].gameObject.SetActive(true);
                Vases[i].transform.position = InitPosition[i];
                Rigidbody rigidbody = Vases[i].GetComponent<Rigidbody>();
                rigidbody.velocity = Vector3.zero;
                rigidbody.isKinematic = true;
                rigidbody.isKinematic = false;
                Vases[i].SetActive(true);
                Debug.Log("has fallado");
            }
            _allowSum = false;
            SelectedOrder = 1;
            Debug.Log("has fallado");
        }
        if(SelectedOrder == NailsInOrder.Length)
        {
            Floorup = true;
        }
        if (_allowSum == true)
        {
            SelectedOrder++;
        }
    }

    
    void Update () {
        if (Floorup == true)
        {
            ScaleA = Vector3.MoveTowards(ScaleA, ScaleB, 20 * Time.deltaTime);
            scaleobject.transform.localScale = ScaleA;
        }

        if (_firstRoutine == true)
        {
            StartCoroutine(PlayAudio());
            _firstRoutine = false;
        }
    }

    //Audio Coroutine
    IEnumerator PlayAudio()
    {
        _floorPlateAudioSource.Play();
        yield return null;
    }
}
