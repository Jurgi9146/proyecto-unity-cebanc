﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoneShootingPosition : MonoBehaviour {

    //Arrow Shooting variable initialization
    [Header("Arrow Shooting")]
    public GameObject _PrefabArrow;
    public Transform _ArrowPositionTransform;
    private bool AllowSecondArrow = true;
    public Vector3 ShootingDirection;
    private bool _FPCOnSkullAre;


    private void Start()
    {
        StartCoroutine(ArrowShootingTimer());
        TriggerSkull TS = FindObjectOfType<TriggerSkull>();
        TS.OnSkullEntered += TS_OnSkullEntered;
    }

    private void TS_OnSkullEntered(bool skullArea)
    {
        _FPCOnSkullAre = skullArea;
    }

    IEnumerator ArrowShootingTimer ()
    {
            yield return new WaitForSeconds(0.2f);
            ArrowShooting();
            StartCoroutine(ArrowShootingTimer());
    }
    private void ArrowShooting()
    {        
        if (_FPCOnSkullAre == true)
        {
            //Instantitate for arrow shooting
            GameObject ArrowShoot = (GameObject)Instantiate(_PrefabArrow, _ArrowPositionTransform.position, Quaternion.Euler(ShootingDirection));
            Rigidbody ArrowRigidBody = ArrowShoot.GetComponentInChildren<Rigidbody>();
            ArrowRigidBody.AddForce(_ArrowPositionTransform.forward * 900f);
        }

    }

 
}
