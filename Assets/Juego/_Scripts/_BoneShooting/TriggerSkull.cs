﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerSkull : MonoBehaviour {

    public delegate void SkullEntered(bool skullArea);
    public event SkullEntered OnSkullEntered;
    private bool _onArea;

    private void Awake()
    {
        OnSkullEntered = null;
    }

    private void OnTriggerEnter(Collider other)
    {
        _onArea = true;
        if (OnSkullEntered !=null)
        {
            OnSkullEntered(_onArea);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        _onArea = false;
        if (OnSkullEntered !=null)
        {
            OnSkullEntered(_onArea);
        }
    }

}
